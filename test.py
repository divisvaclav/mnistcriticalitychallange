# https://github.com/ansh941/MnistSimpleCNN/blob/master/code/train.py
# https://arxiv.org/abs/2008.10400
# imports ---------------------------------------------------------------------#
import sys
import os

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import argparse
import copy
import numpy as np
import json
from tqdm import tqdm
import collections
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from PIL import Image

from libs.datasets import MnistDataset
from libs.models.modelM3 import ModelM3
from libs.models.modelM5 import ModelM5
from libs.models.modelM7 import ModelM7

from libs.visualization import Visualization
import random
from statistics import mean

import matplotlib.pyplot as plt

visualization = Visualization()


def nested_dict():
    return collections.defaultdict(nested_dict)


def calculate_criticality_classification(
        ground_truth: np.ndarray,
        unmasked_conf: np.ndarray,
        masked_conf: np.ndarray,
        unmasked_class: np.ndarray,
        masked_class: np.ndarray,
        criticality_tau: float) -> list:
    criticality = list()
    for gt_cls, um_conf, m_conf, um_cls, m_cls in zip(ground_truth,
                                                      unmasked_conf,
                                                      masked_conf,
                                                      unmasked_class,
                                                      masked_class):
        # adaptation to logSoftMax [0, -inf)
        if gt_cls != um_cls and gt_cls != m_cls:
            criticality.append(um_conf[gt_cls] - m_conf[gt_cls])

        elif gt_cls == um_cls and gt_cls != m_cls:
            if (um_conf[gt_cls] - m_conf[gt_cls]) > 0.5:
                criticality.append(2.0)  # clip the criticality to 2
            else:
                criticality.append(1 / (1 - (um_conf[gt_cls] - m_conf[gt_cls])))

        # anti-critical behavior
        elif gt_cls != um_cls and gt_cls == m_cls:
            criticality.append(um_conf[gt_cls] - m_conf[gt_cls])

        else:
            """ if the new prediction has smaller confidence than original one """
            criticality.append(um_conf[um_cls] - m_conf[m_cls])

    return criticality


def unmask_kernel(_weights, _original_weights, _index: int):
    if len(_weights.shape) == 4:  # to filter only depth separable and conv layers
        _weights[_index, :, :, :] = copy.deepcopy(_original_weights[_index, :, :, :])

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        _weights[_index, :] = copy.deepcopy(_original_weights[_index, :])

    elif len(_weights.shape) == 1:
        _weights[_index] = copy.deepcopy(_original_weights[_index])


def multiple_mask_kernel(_weights, _indices: list):
    if len(_weights.shape) == 4:  # to filter only depth separable and conv layers
        for filters in _indices:
            _weights[filters, :, :, :] = 0.0

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        for neurons in _indices:
            _weights[neurons, :] = 0.0

    elif len(_weights.shape) == 1:
        for neurons in _indices:
            _weights[neurons] = 0.0


def mask_kernel(_weights, _index: int):
    # to filter only depth separable and conv layers
    if len(_weights.shape) == 4:
        _weights[_index, :, :, :] = 0.0

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        _weights[_index, :] = 0.0

    elif len(_weights.shape) == 1:
        _weights[_index] = 0.0


def get_layers_dimension(_weights) -> list():
    if len(_weights.shape) == 4:
        # point-wise
        if _weights.shape[2] == 1 and _weights.shape[3] == 1:
            return [index for index in range(_weights.shape[0])]
        elif _weights.shape[1] == 1:
            return [index for index in range(_weights.shape[0])]
        # depth-wise
        else:
            return [index for index in range(_weights.shape[0])]

    elif len(_weights.shape) == 1:
        return [index for index in range(_weights.shape[0])]

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        return [index for index in range(_weights.shape[0])]

    else:
        return [None]


list_of_analyzed_layers = None


def return_weights(_model, _models_name, _ignore_early_layers=True):
    index_of_first_layer = 0
    if _ignore_early_layers:
        index_of_first_layer = 6

    _parameter_dict = collections.defaultdict(list)
    if _models_name == "detr":
        models = [_model.model.backbone, _model.model.transformer]
    else:
        models = [_model]

    for pytorch_model in models:
        for index, (name, parameter) in enumerate(pytorch_model.named_parameters()):
            if index >= index_of_first_layer:
                if list_of_analyzed_layers is not None:
                    # TBD add biases and mask them simultaneuosly
                    if 'weight' in name and name in list_of_analyzed_layers:
                        _parameter_dict[name].append(parameter)
                else:
                    if 'weight' in name:
                        _parameter_dict[name].append(parameter)

    return _parameter_dict


def find_critical_neurons(model="M3", p_logdir="temp", dependency_masking=False):
    # enable GPU usage ------------------------------------------------------------#
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    if use_cuda == False:
        print("WARNING: CPU will be used for evaluation.")

    # data loader -----------------------------------------------------------------#
    test_dataset = MnistDataset(training=False, transform=None)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=100, shuffle=False)

    sorted_dataset = collections.defaultdict(list)
    for data, target in test_dataset:
        sorted_dataset[str(target.item())].append(data.numpy())

    anti_critical_path = collections.defaultdict(lambda: collections.defaultdict(list))

    for target, data in sorted_dataset.items():

        print("Analyzing number: {}".format(target))
        # model selection -------------------------------------------------------------#
        if model == "M3":
            model1 = ModelM3().to(device)
        elif model == "M5":
            model1 = ModelM5().to(device)
        elif model == "M7":
            model1 = ModelM7().to(device)

        path_to_log = os.path.join("logs", model)

        model1.load_state_dict(torch.load(os.path.join(path_to_log, "best_model.pth")))

        model1.eval()

        json_logging_path = path_to_log
        if not os.path.exists(json_logging_path):
            os.makedirs(json_logging_path)

        param = return_weights(model1, "custom")

        with torch.no_grad():
            # casting it in tensor
            data = torch.Tensor(np.asarray(data)).to(device)
            target_torch = torch.linspace(float(target), float(target), steps=data.shape[0]).to(device)
            # doing prediction
            temp_um_conf = model1(data)
            um_cls = temp_um_conf.cpu().argmax(dim=1, keepdim=True).numpy()
            um_conf = temp_um_conf.cpu().numpy()

            # eval acc on unmasked cnn
            pred = temp_um_conf.cpu().argmax(dim=1, keepdim=True)
            correct = pred.eq(target_torch.cpu().view_as(pred)).sum().item()
            max_test_accuracy = 100 * correct / data.shape[0]

            layers_criticality_dict = nested_dict()

            for layers_name, param in tqdm(param.items()):

                original_values = copy.deepcopy(param[0].data)
                original_values.to(device)
                masked_values = param[0].data

                kernels = get_layers_dimension(param[0].data)
                '''
                ### 3) Iterating over kernels
                '''
                for kernel_index in kernels:
                    # mask the kernel
                    mask_kernel(masked_values, int(kernel_index))

                    temp_m_conf = model1(data)
                    m_cls = temp_m_conf.cpu().argmax(dim=1, keepdim=True).numpy()
                    m_conf = temp_m_conf.cpu().numpy()

                    # evaluate the criticality of the kernel
                    cri = calculate_criticality_classification(
                        np.asarray(target_torch.cpu().numpy(), dtype=int),
                        um_conf, m_conf,
                        um_cls.flatten(), m_cls.flatten(), 0.0)

                    # unmasking the weights
                    unmask_kernel(masked_values, original_values, int(kernel_index))

                    cri_str = [str(c) for c in cri]
                    layers_criticality_dict[target][layers_name][str(kernel_index)] = cri_str

                if dependency_masking:
                    float_array = np.asarray(list(layers_criticality_dict[target][layers_name].values()),
                                             dtype=float)
                    cri_stds = np.std(float_array, axis=1)
                    cri_mean = np.mean(float_array, axis=1)
                    anti_critical_mean = cri_mean < 0.0
                    if True in anti_critical_mean:
                        anti_critical_neurons_indices = np.where(anti_critical_mean == True)
                        criticality_neurons_indices = (cri_mean + cri_stds)[anti_critical_neurons_indices]
                        list_of_sorted_indices = list(np.argsort(criticality_neurons_indices))
                        most_anti_critical_kernel = [list_of_sorted_indices.index(all_indices) for all_indices in
                                                     range(len(list_of_sorted_indices))]
                        # print(most_anti_critical_kernel)

                        for first_x_anticritical in most_anti_critical_kernel[
                                                    :np.minimum(len(most_anti_critical_kernel), 5)]:
                            # taking the one neuron with the smallest criticality
                            mask_kernel(masked_values, int(first_x_anticritical))
                            print("\nMasking layer: {}, kernel: {}".format(layers_name, str(first_x_anticritical)))

                            # eval loss and acc on masked cnn
                            test_loss = F.nll_loss(temp_m_conf.cpu(), target_torch.cpu().type(torch.LongTensor),
                                                   reduction='sum').item()
                            pred = temp_m_conf.cpu().argmax(dim=1, keepdim=True)
                            correct = pred.eq(target_torch.cpu().view_as(pred)).sum().item()
                            test_loss /= data.shape[0]
                            test_accuracy = 100 * correct / data.shape[0]

                            # remember the path only in case the accuracy was better
                            if test_accuracy > max_test_accuracy:
                                max_test_accuracy = test_accuracy
                                anti_critical_path[target][layers_name].append(str(first_x_anticritical))
                                print('Number: {} Test set: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)'.format(
                                    target, test_loss, correct, data.shape[0], test_accuracy))
                            else:
                                unmask_kernel(masked_values, original_values, int(first_x_anticritical))

                with open(os.path.join(json_logging_path, target + "_criticality_export.json"),
                          'w') as json_statistics:
                    json.dump(layers_criticality_dict, json_statistics)

        with open(os.path.join(json_logging_path, target + "_anti_critical_path_export.json"),
                  'w') as json_statistics:
            json.dump(anti_critical_path, json_statistics)


def analyze_critical_neurons(json_logging_path):
    anti_critical_neurons = nested_dict()
    for target in range(10):

        target = str(target)
        with open(os.path.join(json_logging_path, target + "_criticality_export.json"), 'r') as json_statistics:
            statistics = json.load(json_statistics)

        for gt, layers in statistics.items():

            fig_dir = os.path.join(json_logging_path, "criticality", gt)
            if not os.path.exists(fig_dir):
                os.makedirs(fig_dir)

            list_of_optimal_std = []

            for layer, kernels in layers.items():
                kernels_cri = list()
                for kernel, criticality in kernels.items():
                    kernels_cri.append(criticality)

                float_array = np.asarray(kernels_cri, dtype=float)
                cri_stds = np.std(float_array, axis=1)
                cri_mean = np.mean(float_array, axis=1)
                num_anti_critical_neurons = 1000
                optimal_std = 1
                criticality_neurons_indices = np.argsort(cri_mean + cri_stds)

                list_anti_criticality_different_std = {}
                for weight in np.linspace(1,0.001,1000).tolist():
                    anti_critical_mean = cri_mean + weight * cri_stds < 0.0
                    if True in anti_critical_mean:
                        anti_critical_neurons_indices = np.where(anti_critical_mean == True)

                        if len(anti_critical_neurons_indices[0]) == 1:
                            optimal_std = weight
                        list_anti_criticality_different_std[weight] = anti_critical_neurons

                anti_critical_mean = cri_mean + optimal_std * cri_stds < 0.0
                anti_critical_neurons_indices = np.where(anti_critical_mean==True)
                sorted_anti_critical = np.sort(criticality_neurons_indices[anti_critical_neurons_indices])
                anti_critical_neurons[str(target)][layer] = sorted_anti_critical.tolist()
                list_of_optimal_std.append(optimal_std)
            _optimal_std = mean(list_of_optimal_std)
            print(_optimal_std)
    return anti_critical_neurons


def analyse_depth_anti_criticality(specific_target, model, anti_critical_neurons):

    device = 'cpu'
    test_dataset = MnistDataset(training=False, transform=None)

    sorted_dataset = collections.defaultdict(list)
    for data, target in test_dataset:
        if target == specific_target:
            sorted_dataset[str(target.item())].append(data.numpy())

    for target, data in sorted_dataset.items():

        print("Analyzing number: {}".format(target))
        # model selection -------------------------------------------------------------#
        if model == "M3":
            model1 = ModelM3().to(device)
        elif model == "M5":
            model1 = ModelM5().to(device)
        elif model == "M7":
            model1 = ModelM7().to(device)

        path_to_log = os.path.join("logs", model)

        model1.load_state_dict(torch.load(os.path.join(path_to_log, "best_model.pth"), map_location=torch.device('cpu')))

        model1.eval()

        param = return_weights(model1, "custom")

        # compute and store the number of kernals in each layer
        num_kernals_each_layer = collections.defaultdict(list)
        for layers_name, param in param.items():
            num_kernals_each_layer[layers_name] = len(get_layers_dimension(param[0].data))

        ant_criticality_depth_analysis = []
        for layers_name, anti_crit_neurons_in_layer in anti_critical_neurons[str(specific_target)].items():
            ant_criticality_depth_analysis.append(len(anti_crit_neurons_in_layer)/num_kernals_each_layer[layers_name])

        stat = ant_criticality_depth_analysis

    return stat


def get_specialsed_models(model):

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    # model selection -------------------------------------------------------------#
    if model == "M3":
        model1 = ModelM3().to(device)
    elif model == "M5":
        model1 = ModelM5().to(device)
    elif model == "M7":
        model1 = ModelM7().to(device)

    path_to_log = os.path.join("logs", model)

    model1.load_state_dict(torch.load(os.path.join(path_to_log, "best_model.pth"), map_location=torch.device('cpu')))

    model1.eval()

    _param = return_weights(model1, "custom")

    # data loader -----------------------------------------------------------------#
    test_dataset = MnistDataset(training=False, transform=None)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=100, shuffle=False)

    optimal_anti_critical_layer = nested_dict()


    for target in range(10):
        sorted_dataset = collections.defaultdict(list)
        for data, _target in test_dataset:
            if str(_target.numpy()) == str(target):
                sorted_dataset[str(_target.numpy())].append(data.numpy())

    for target in range(10):
        with torch.no_grad():
            # casting it in tensor
            data = torch.Tensor(np.asarray(sorted_dataset[str(target)])).to(device)
            target_torch = torch.linspace(float(target), float(target), steps=data.shape[0]).to(device)
            # doing prediction
            temp_um_conf = model1(data)
            um_cls = temp_um_conf.cpu().argmax(dim=1, keepdim=True).numpy()
            um_conf = temp_um_conf.cpu().numpy()

            # eval acc on unmasked cnn
            pred = temp_um_conf.cpu().argmax(dim=1, keepdim=True)
            correct = pred.eq(target_torch.cpu().view_as(pred)).sum().item()
            um_test_accuracy = 100 * correct / data.shape[0]

            print(um_test_accuracy)

            dict_original_values = {}
            dict_masked_values = {}

            anti_critical_neurons = analyze_critical_neurons(path_to_log, str(target))

            for layers_name, param in _param.items():

                original_values = copy.deepcopy(param[0].data)
                dict_original_values[layers_name] = original_values
                original_values.to(device)
                dict_masked_values[layers_name] = param[0].data


                if layers_name != 'fc1.weight' and layers_name != 'fc1_bn.weight':
                    indices = anti_critical_neurons[str(target)][layers_name].tolist()
                    selected_indices = random.sample(indices, int(len(indices) * 1))
                    multiple_mask_kernel(dict_masked_values[layers_name], selected_indices)
                    temp_m_conf = model1(data)
                    m_cls = temp_m_conf.cpu().argmax(dim=1, keepdim=True).numpy()
                    m_conf = temp_m_conf.cpu().numpy()

                    # eval acc on unmasked cnn
                    pred = temp_m_conf.cpu().argmax(dim=1, keepdim=True)
                    correct = pred.eq(target_torch.cpu().view_as(pred)).sum().item()
                    m_test_accuracy = 100 * correct / data.shape[0]

                    for index in selected_indices:
                        unmask_kernel(dict_masked_values[layers_name], dict_original_values[layers_name], index)

                    print(str(target) + ' ' + model)
                    print(m_test_accuracy)

                    if um_test_accuracy <= m_test_accuracy:
                        print(layers_name)
                        print(selected_indices)
                        if m_test_accuracy == 100.0:
                            optimal_anti_critical_layer[target][model][layers_name] = selected_indices

    return optimal_anti_critical_layer


def get_accuracy_masked_anticritical_kernals(model):
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    # model selection -------------------------------------------------------------#
    if model == "M3":
        model1 = ModelM3().to(device)
    elif model == "M5":
        model1 = ModelM5().to(device)
    elif model == "M7":
        model1 = ModelM7().to(device)

    path_to_log = os.path.join("logs", model)

    model1.load_state_dict(torch.load(os.path.join(path_to_log, "best_model.pth"), map_location=torch.device('cpu')))

    model1.eval()

    _param = return_weights(model1, "custom")

    # data loader -----------------------------------------------------------------#
    test_dataset = MnistDataset(training=False, transform=None)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=100, shuffle=False)

    optimal_anti_critical_layer = nested_dict()

    for target in range(10):
        sorted_dataset = collections.defaultdict(list)
        for data, _target in test_dataset:
            if str(_target.numpy()) == str(target):
                sorted_dataset[str(_target.numpy())].append(data.numpy())

        with torch.no_grad():
            # casting it in tensor
            data = torch.Tensor(np.asarray(sorted_dataset[str(target)])).to(device)
            target_torch = torch.linspace(float(target), float(target), steps=data.shape[0]).to(device)
            # doing prediction
            temp_um_conf = model1(data)
            um_cls = temp_um_conf.cpu().argmax(dim=1, keepdim=True).numpy()
            um_conf = temp_um_conf.cpu().numpy()

            # eval acc on unmasked cnn
            pred = temp_um_conf.cpu().argmax(dim=1, keepdim=True)
            correct = pred.eq(target_torch.cpu().view_as(pred)).sum().item()
            um_test_accuracy = 100 * correct / data.shape[0]

            print(um_test_accuracy)

            dict_original_values = {}
            dict_masked_values = {}

            anti_critical_neurons = analyze_critical_neurons(path_to_log, str(target))

            for layers_name, param in _param.items():

                original_values = copy.deepcopy(param[0].data)
                dict_original_values[layers_name] = original_values
                original_values.to(device)
                dict_masked_values[layers_name] = param[0].data

                if layers_name != 'fc1.weight' and layers_name != 'fc1_bn.weight':
                    indices = anti_critical_neurons[str(target)][layers_name].tolist()
                    selected_indices = random.sample(indices, int(len(indices) * 1))
                    multiple_mask_kernel(dict_masked_values[layers_name], selected_indices)
                    temp_m_conf = model1(data)
                    m_cls = temp_m_conf.cpu().argmax(dim=1, keepdim=True).numpy()
                    m_conf = temp_m_conf.cpu().numpy()

                    # eval acc on unmasked cnn
                    pred = temp_m_conf.cpu().argmax(dim=1, keepdim=True)
                    correct = pred.eq(target_torch.cpu().view_as(pred)).sum().item()
                    m_test_accuracy = 100 * correct / data.shape[0]

                    for index in selected_indices:
                        unmask_kernel(dict_masked_values[layers_name], dict_original_values[layers_name], index)

                    print(str(target) + ' ' + model)
                    print(m_test_accuracy)

                    if um_test_accuracy <= m_test_accuracy:
                        print(layers_name)
                        print(selected_indices)
                        optimal_anti_critical_layer[target][model][layers_name] = selected_indices

    return optimal_anti_critical_layer


def get_accuracy_masked_commun_anti_critical_kernals(model):
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    # model selection -------------------------------------------------------------#
    if model == "M3":
        model1 = ModelM3().to(device)
    elif model == "M5":
        model1 = ModelM5().to(device)
    elif model == "M7":
        model1 = ModelM7().to(device)

    path_to_log = os.path.join("logs", model)

    model1.load_state_dict(torch.load(os.path.join(path_to_log, "best_model.pth"), map_location=torch.device('cpu')))

    model1.eval()

    _param = return_weights(model1, "custom")

    # data loader -----------------------------------------------------------------#
    test_dataset = MnistDataset(training=False, transform=None)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=100, shuffle=False)

    list_anti_critical_neurons = []
    optimal_anti_critical_layer = nested_dict()
    m_acc_list = []
    um_acc_list = []
    for i in range(10):
        list_anti_critical_neurons.append(analyze_critical_neurons(path_to_log, str(i)))

    sorted_dataset = collections.defaultdict(list)
    for data, _target in test_dataset:
        sorted_dataset[str(_target.numpy())].append(data.numpy())

    commun_anti_critical_kernals = nested_dict()
    for layers_name, param in _param.items():
        if layers_name != 'fc1.weight' and layers_name != 'fc1_bn.weight':
            list_common_index = list(set(list_anti_critical_neurons[0][str(0)][layers_name].tolist()).intersection(list_anti_critical_neurons[1][str(1)][layers_name].tolist()))
            for target in range(1, 9):
                list_common_index = list(set(list_common_index).intersection(list_anti_critical_neurons[target+1][str(target+1)][layers_name].tolist()))
            commun_anti_critical_kernals[layers_name] = list_common_index

    for target in range(10):

        with torch.no_grad():
            # casting it in tensor
            data = torch.Tensor(np.asarray(sorted_dataset[str(target)])).to(device)
            target_torch = torch.linspace(float(target), float(target), steps=data.shape[0]).to(device)
            # doing prediction
            temp_um_conf = model1(data)
            um_cls = temp_um_conf.cpu().argmax(dim=1, keepdim=True).numpy()
            um_conf = temp_um_conf.cpu().numpy()

            # eval acc on unmasked cnn
            pred = temp_um_conf.cpu().argmax(dim=1, keepdim=True)
            correct = pred.eq(target_torch.cpu().view_as(pred)).sum().item()
            um_test_accuracy = 100 * correct / data.shape[0]

            um_acc_list.append(um_test_accuracy)

            print(um_test_accuracy)

            dict_original_values = {}
            dict_masked_values = {}

            anti_critical_neurons = analyze_critical_neurons(path_to_log, str(target))
            best_acc = 0
            for layers_name, param in _param.items():

                original_values = copy.deepcopy(param[0].data)
                dict_original_values[layers_name] = original_values
                original_values.to(device)
                dict_masked_values[layers_name] = param[0].data

                if layers_name != 'fc1.weight' and layers_name != 'fc1_bn.weight':
                    indices = commun_anti_critical_kernals[layers_name]
                    selected_indices = random.sample(indices, int(len(indices) * 1))
                    multiple_mask_kernel(dict_masked_values[layers_name], selected_indices)
                    temp_m_conf = model1(data)
                    m_cls = temp_m_conf.cpu().argmax(dim=1, keepdim=True).numpy()
                    m_conf = temp_m_conf.cpu().numpy()

                    # eval acc on unmasked cnn
                    pred = temp_m_conf.cpu().argmax(dim=1, keepdim=True)
                    correct = pred.eq(target_torch.cpu().view_as(pred)).sum().item()
                    m_test_accuracy = 100 * correct / data.shape[0]

                    if um_test_accuracy <= m_test_accuracy:
                        if m_test_accuracy > best_acc:
                            best_acc = m_test_accuracy

                        print(layers_name)
                        print(target)
                        print(selected_indices)
                        print(m_test_accuracy)

                    #else:
                    #    for index in selected_indices:
                    #        unmask_kernel(dict_masked_values[layers_name], dict_original_values[layers_name], index)
            m_acc_list.append(best_acc)

    print(m_acc_list)
    print(um_acc_list)

    m_final_acc = sum(m_acc_list)/10
    um_final_acc = sum(um_acc_list) / 10

    print(m_final_acc)
    print(um_final_acc)




    return um_final_acc

if __name__ == "__main__":
    path_to_log = os.path.join("logs", "M3")
    p = argparse.ArgumentParser()
    p.add_argument("--logdir", default="M3")
    p.add_argument("--seed", default=0, type=int)
    p.add_argument("--trials", default=10, type=int)
    p.add_argument("--kernel_size", default=3, type=int)
    args = p.parse_args()

    #find_critical_neurons(model="M3", p_logdir="temp", dependency_masking=False)
    anti_critical_neurons = analyze_critical_neurons(path_to_log)
    print(anti_critical_neurons)

    with open(path_to_log + 'single_neuron_anti_critical_masking_M3.json', 'w') as f:
        json.dump(anti_critical_neurons, f)
    #get_accuracy_masked_commun_anti_critical_kernals('M5')

