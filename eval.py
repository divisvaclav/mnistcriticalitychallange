import os

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import copy
import numpy as np
import json
import collections
import torch
import torch.nn.functional as F
from PIL import Image

from libs.datasets import MnistDataset
from libs.models.modelM3 import ModelM3
from libs.models.modelM5 import ModelM5
from libs.models.modelM7 import ModelM7

from libs.visualization import Visualization

visualization = Visualization()

def nested_dict():
    return collections.defaultdict(nested_dict)

def unmask_kernel(_weights, _original_weights, _index: int):
    if len(_weights.shape) == 4:  # to filter only depth separable and conv layers
        _weights[_index, :, :, :] = copy.deepcopy(_original_weights[_index, :, :, :])

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        _weights[_index, :] = copy.deepcopy(_original_weights[_index, :])

    elif len(_weights.shape) == 1:
        _weights[_index] = copy.deepcopy(_original_weights[_index])


def multiple_mask_kernel(_weights, _indices: list):
    if len(_weights.shape) == 4:  # to filter only depth separable and conv layers
        for filters in _indices:
            _weights[filters, :, :, :] = 0.0

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        for neurons in _indices:
            _weights[neurons, :] = 0.0

    elif len(_weights.shape) == 1:
        for neurons in _indices:
            _weights[neurons] = 0.0


def mask_kernel(_weights, _index: int):
    # to filter only depth separable and conv layers
    if len(_weights.shape) == 4:
        _weights[_index, :, :, :] = 0.0

    # to filter only dense layers
    elif len(_weights.shape) == 2:
        _weights[_index, :] = 0.0

    elif len(_weights.shape) == 1:
        _weights[_index] = 0.0


list_of_analyzed_layers = None


def return_weights(_model, _models_name, _ignore_early_layers=True):
    index_of_first_layer = 0
    if _ignore_early_layers:
        index_of_first_layer = 6

    _parameter_dict = collections.defaultdict(list)
    if _models_name == "detr":
        models = [_model.model.backbone, _model.model.transformer]
    else:
        models = [_model]

    for pytorch_model in models:
        for index, (name, parameter) in enumerate(pytorch_model.named_parameters()):
            if index >= index_of_first_layer:
                if list_of_analyzed_layers is not None:
                    # TBD add biases and mask them simultaneuosly
                    if 'weight' in name and name in list_of_analyzed_layers:
                        _parameter_dict[name].append(parameter)
                else:
                    if 'weight' in name:
                        _parameter_dict[name].append(parameter)

    return _parameter_dict


def evaluate():

    models_list = ['M3', 'M5', 'M7']
    best_30_specialized_models = nested_dict()

    # enable GPU usage ------------------------------------------------------------#
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    if use_cuda == False:
        print("WARNING: CPU will be used for evaluation.")

    # data loader -----------------------------------------------------------------#
    test_dataset = MnistDataset(training=False, transform=None)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1, shuffle=False)

    # model selection -------------------------------------------------------------#




    for model in models_list:#
        path_to_log = os.path.join("logs", model, "single_neuron_anti_critical_masking_" + model + ".json")

        json_file = open(path_to_log)

        anti_critical_neurons = json.load(json_file)
        if model == "M3":
            model1 = ModelM3().to(device)
            path_to_log = os.path.join("logs", "M3")
        elif model == "M5":
            model1 = ModelM5().to(device)
            path_to_log = os.path.join("logs", "M5")
        elif model == "M7":
            model1 = ModelM7().to(device)
            path_to_log = os.path.join("logs", "M7")

        path_to_images = os.path.join(path_to_log, "wrong_images")
        model1.load_state_dict(torch.load(os.path.join(path_to_log, "best_model.pth"), map_location=torch.device('cpu')))

        model1.eval()

        parameter_dict = return_weights(model1, "custom")

        sorted_dataset = collections.defaultdict(list)
        for data, target in test_dataset:
            sorted_dataset[str(target.item())].append(data.numpy())

        with torch.no_grad():

            path_to_images = os.path.join(path_to_log, "original", "wrong_images")
            for batch_idx, (target, data) in enumerate(sorted_dataset.items()):
                test_loss = 0
                correct = 0
                wrong_images = []

                # prepare data
                data = torch.Tensor(np.asarray(data)).to(device)
                target_torch = torch.linspace(float(target), float(target), steps=data.shape[0]).to(device)
                output = model1(data)
                test_loss += F.nll_loss(output, target_torch.type(torch.LongTensor), reduction='sum').item()
                pred = output.argmax(dim=1, keepdim=True)
                correct += pred.eq(target_torch.view_as(pred)).sum().item()
                wrong_images.extend(
                    np.nonzero(~pred.eq(target_torch.view_as(pred)).cpu().numpy())[0] + (1 * batch_idx))  #
                wrongs = list(np.nonzero(~pred.eq(target_torch.view_as(pred)).cpu().numpy())[0])

                # save wrongly predicted images
                if len(wrongs) > 0:
                    path_to_wrong_images = os.path.join(path_to_images, target)
                    if not os.path.exists(path_to_wrong_images):
                        os.makedirs(path_to_wrong_images)
                    for wrong in wrongs:
                        img = Image.fromarray(np.uint8(data[wrong].cpu().numpy() * 255)[0])
                        img.save(os.path.join(path_to_wrong_images, "wrong%05d.jpeg" % (wrong + (1 * batch_idx))))

                test_loss /= data.shape[0]
                test_accuracy = 100 * correct / data.shape[0]
                print('Number: {} Test set: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)'.format(
                    target, test_loss, correct, data.shape[0], test_accuracy))

                np.savetxt(os.path.join(path_to_images, "number_{}_wrong.txt".format(target)), wrong_images, fmt="%d")

            # find the best combination of anti-critical neurons
            for target, layers in anti_critical_neurons.items():

                data = sorted_dataset[str(target)]

                max_test_accuracy = 0.0
                max_target = 0.0
                max_test_loss = 0.0
                max_correct = 0
                data_len = 0
                best_layer = "none"
                best_kernel = "none"

                # for all layers
                for layer, kernels in layers.items():

                    param = parameter_dict[layer]
                    original_values = copy.deepcopy(param[0].data)
                    original_values.to(device)

                    # for each kernel
                    for kernel in kernels:
                        path_to_images = os.path.join(path_to_log, layer + "_" + str(kernel), "wrong_images")

                        masked_values = param[0].data
                        mask_kernel(masked_values, int(kernel))

                        data = torch.Tensor(np.asarray(data)).to(device)
                        target_torch = torch.linspace(float(target), float(target), steps=data.shape[0]).to(device)
                        output = model1(data)
                        test_loss = F.nll_loss(output, target_torch.type(torch.LongTensor), reduction='sum').item()
                        pred = output.argmax(dim=1, keepdim=True)
                        correct = pred.eq(target_torch.view_as(pred)).sum().item()
                        wrong_images = np.nonzero(~pred.eq(target_torch.view_as(pred)).cpu().numpy())[0] + (1 * batch_idx)
                        wrongs = list(np.nonzero(~pred.eq(target_torch.view_as(pred)).cpu().numpy())[0])

                        if len(wrongs) > 0:
                            path_to_wrong_images = os.path.join(path_to_images, target)
                            if not os.path.exists(path_to_wrong_images):
                                os.makedirs(path_to_wrong_images)
                            for wrong in wrongs:
                                img = Image.fromarray(np.uint8(data[wrong].cpu().numpy() * 255)[0])
                                img.save(os.path.join(path_to_wrong_images,
                                                      "wrong%05d.jpeg" % (wrong + (1 * batch_idx))))

                        # saving the best network performance
                        test_loss /= data.shape[0]
                        test_accuracy = 100 * correct / data.shape[0]
                        if max_test_accuracy < test_accuracy:
                            max_test_accuracy = test_accuracy
                            best_layer = layer
                            best_kernel = str(kernel)

                        if max_correct < correct:
                            max_correct = correct

                        if max_test_loss < test_loss:
                            max_test_loss = test_loss

                        data_len = data.shape[0]
                        max_target = target

                        # np.savetxt(os.path.join(path_to_images, "number_{}_wrong.txt".format(target)), wrong_images, fmt="%d")
                        # print(len(wrong_images), wrong_images)
                        # unmasking the weights
                        unmask_kernel(masked_values, original_values, int(kernel))


                print('Number: {} Test set: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)'.format(
                    max_target, max_test_loss, max_correct, data_len, max_test_accuracy))
                print("Layer: {}, index:{}".format(best_layer, best_kernel))

                best_30_specialized_models[target][model][best_layer] = best_kernel
                print(best_30_specialized_models)

    return best_30_specialized_models

def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3


def FP_evaluation(gt):

    path_to_log = os.path.join('logs30_specialized_models.json')

    json_file = open(path_to_log)

    masks_for_specialized_models = json.load(json_file)

    # enable GPU usage ------------------------------------------------------------#
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    if use_cuda == False:
        print("WARNING: CPU will be used for evaluation.")

    # data loader -----------------------------------------------------------------#
    test_dataset = MnistDataset(training=False, transform=None)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=10000, shuffle=False)

    sorted_dataset = collections.defaultdict(list)
    #for data, target in test_dataset:
    #    sorted_dataset[str(target.item())].append(data.numpy())
    # model selection -------------------------------------------------------------#

    list_models = ['M3', 'M5', 'M7']
    res_pairwise_intersections = []


    for model in list_models:
        if model == "M3":
            model1 = ModelM3().to(device)
        elif model == "M5":
            model1 = ModelM5().to(device)
        elif model == "M7":
            model1 = ModelM7().to(device)

        path_to_log = os.path.join("logs", model)

        model1.load_state_dict(
            torch.load(os.path.join(path_to_log, "best_model.pth"), map_location=torch.device('cpu')))

        model1.eval()

        specilized_model = model1
        specilized_model.to(device)

        specilized_model.eval()


        mask_layer = list(masks_for_specialized_models[str(gt)][model].keys())[0]
        mask_index = list(masks_for_specialized_models[str(gt)][model].values())[0]

        param = return_weights(specilized_model, "custom")


        masked_values = param[mask_layer][0].data
        mask_kernel(masked_values, int(mask_index))

        FP_indices = []
        FP_indices_spe = []

        for data, target in test_loader:
            with torch.no_grad():
                # casting it in tensor
                data = torch.Tensor(np.asarray(data)).to(device)
                target.to(device)
                #target_torch = torch.linspace(float(target), float(target), steps=data.shape[0]).to(device)
                # doing prediction
                temp_um_conf = model1(data)
                um_cls = temp_um_conf.cpu().argmax(dim=1, keepdim=True).numpy()
                um_conf = temp_um_conf.cpu().numpy()

                # eval acc on unmasked cnn
                pred = temp_um_conf.cpu().argmax(dim=1, keepdim=True)

                correct = pred.eq(target.cpu().view_as(pred)).sum().item()
                max_test_accuracy = 100 * correct / data.shape[0]
                pred = np.reshape(np.asarray(pred.tolist()), 10000).tolist()
                #res_prediction.append(np.reshape(np.asarray(pred.tolist()),10000).tolist())
                #list_gts.append(target.tolist())

                for index, element in enumerate(pred):
                    if pred[index] == gt and target.tolist()[index] != gt:
                        FP_indices.append(index)

                temp_um_conf = specilized_model(data)
                um_cls = temp_um_conf.cpu().argmax(dim=1, keepdim=True).numpy()
                um_conf = temp_um_conf.cpu().numpy()

                # eval acc on unmasked cnn
                pred = temp_um_conf.cpu().argmax(dim=1, keepdim=True)

                correct = pred.eq(target.cpu().view_as(pred)).sum().item()
                max_test_accuracy = 100 * correct / data.shape[0]
                pred = np.reshape(np.asarray(pred.tolist()), 10000).tolist()
                # res_prediction.append(np.reshape(np.asarray(pred.tolist()),10000).tolist())
                # list_gts.append(target.tolist())

                for index, element in enumerate(pred):
                    if pred[index] == gt and target.tolist()[index] != gt:
                        FP_indices_spe.append(index)
                res_pairwise_intersection = intersection(FP_indices_spe, FP_indices)
                res_pairwise_intersections.append(res_pairwise_intersection)

                print("pairwise intersection between a model and its derived specialized: ")
                print(res_pairwise_intersection)

    common_FP_all_models = intersection(res_pairwise_intersections[0], res_pairwise_intersections[1])
    common_FP_all_models = intersection(common_FP_all_models, res_pairwise_intersections[2])

    return common_FP_all_models


if __name__ == "__main__":

    path_to_log = os.path.join("logs")
    #best_30_specialized_models = evaluate()
    #with open(path_to_log + '30_specialized_models.json', 'w') as f:
    #    json.dump(best_30_specialized_models, f)

    res = FP_evaluation(7)
    print(res)
